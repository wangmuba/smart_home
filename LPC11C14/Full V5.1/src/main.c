/*******************************************************************************
* File:    main.c 
* Author:  Farsight Design Team
* Version: V1.00
* Date:    2011.06.21
* Brief:   Main program body
********************************************************************************
********************************************************************************
* History:
* 2011.05.10:  V1.00		   initial version
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>	
#include "lpc11xx.h"
#include "clkconfig.h"
#include "gpio.h"
#include "uart.h"
#include "timer32.h"
#include "ssp.h"
#include "i2c.h"
#include "wdt.h"
#include "adc.h"
#include "rom_drivers.h"

#include "seg7led.h"
#include "oled2864.h" 
#include "light.h"
#include "acc.h"
#include "key.h"
#include "rfid.h"
#include "tools.h"
#include "spi_uart.h"
#include "temp_hum.h"
#include "collect_data.h"
#include "led_spk_fan.h"
#include "myrfid.h"
#include "menu.h"
#include "data_type.h"
#include "file_operation.h"
#include "sys_init.h"


uint8_t 		STORAGE_NUM = 0;  			  //设备号（发送开机命令后由上层节点返回）
message_tag_t   message_tag_s;
com_t   com_s;   //定义结构体
tem_t 			tem_s;
hum_t 			hum_s;
light_t 		light_s;
acc_t 			acc_s;
adc_t       	adc_s;
state_t 		state_s;
rfid_t 			rfid_s;
command_t 		command_s;
key_t 			key_s;
env_msg_t 		env_msg_s;
data_t			data_s;
message_t 		message_s;
message_t  		message_r;
 	  	
volatile uint32_t counter1 = 0;
volatile uint32_t counter2 = 0;
static uint8_t		   cnt = 0;

extern char zigbee_flag;
extern char rfid_flag;
extern char up_flag;
extern char down_flag;
extern char left_flag;
extern char right_flag;
extern char esc_flag;
extern char sel_flag;
extern uint8_t numb;
extern uint8_t	SPI752_rbuf_1[];

uint8_t  k = 0;
uint8_t  j = 0;
uint8_t	 rx[48] = {0};
uint8_t  rbuf[48] = {0};    //接收数据，存取卡里面的数据
uint8_t  *p;
uint32_t rfid_id;
char 	 dis_buf[30] = {0};





const unsigned char  aucCRCHi[] = {

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,

    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 

    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,

    0x00, 0xC1, 0x81, 0x40

};

const unsigned char aucCRCLo[] = {

    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,

0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,

    0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,

    0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,

    0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,

    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,

    0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,

    0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 

    0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,

    0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,

    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,

    0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,

    0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 

    0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,

    0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,

    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,

    0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,

    0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,

    0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,

    0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,

    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,

    0x41, 0x81, 0x80, 0x40
};

/*******************************************************************************
* Function Name  : SysTick_Handler
* Description    : SysTick interrupt Handler.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SysTick_Handler(void)
{
  static uint32_t	Tick=0;
  
  Tick ++;
  if(Tick == 500)
  {
    GPIOSetValue(PORT3, 1, 0);		// PIO3_1 output 1, turn on LED2
//	Seg7Led_Put(cnt);
	cnt ++;
	cnt &= 0x1f;
  }
  else if(Tick >= 1000)
  {
  	counter1++;
	counter2++;
    GPIOSetValue(PORT3, 1, 1);		// PIO3_1 output 1, turn off LED2
	Tick = 0;
  }
}




int main(void)
{

	
	
	uint8_t	rd;
	uint8_t password[4];   //存放密码
	uint8_t pass[]="Please enter password";
	uint8_t passerror[]="Passeord error,please re-enter";
//	int8_t	x = 2;    // typedef   signed          char int8_t;
//	int8_t	y = 0;
	int m=0;
  int n=0;
	int k=0;
	int L=0;
	//int f=0;
 // static int s=0;
  int8_t 	i = 0;
  int8_t	j = 6;
	//uint32_t	lux_num;
	uint8_t	rbuf[36];
//uint8_t	buf[12] = "ZigBee Test:";
uint8_t RFID_WRITE_DATA_BLOCK_22[26]={0x00};
uint8_t RFID_WRITE_DATA_BLOCK_22_1[10] = {0x1a, 0x22, 0x00, 0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
uint8_t RFID_WRITE_DATA_BLOCK_22_2[6] = {0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
	p = rx;
	init();
 
 
	
	STORAGE_NUM = 1;
	

	
	while(1)
    {
				
			if(esc_flag)     //判断是否按门铃
			{	
				esc_flag = 0;
				memset(&com_s,com_len,0);   //清空message结构体
				com_s.a='b';   //门铃
				com_s.b='a';		//关闭				
			  memcpy(&com_s.str,"0123456789",10);   //将rbuf内存区的数据复制到	
				send_message_zigbee(&com_s);   //发送数据			
			}
			
			
			
			if(rfid_flag)    //判断是否刷卡，刷卡时会产生中断将rfid_flag置1
			{
					rfid_flag = 0;  
					if(Rfid_Operation(STORAGE_NUM,rbuf))    //判断卡中是否有数据,并将数据存入buf中
					{  
						memset(&com_s,com_len,0);   //清空message数组
						com_s.a='c';   //刷卡
						com_s.b='a';		//关门				
			      memcpy(&com_s.str,rbuf+2,sizeof(rbuf));   //将rbuf内存区的数据复制到	
						send_message_zigbee(&com_s);   //发送数据						
					} 
					
					GPIOIntEnable(PORT2,8);
			}
	
			
			
			if(zigbee_flag)   //zigbee接收数据
			{
				zigbee_flag=0;
				
				if(ZigBee_GetChar(&rd))    //判断zigbee是否收到信号   unsigned char rd；rd保存接收的数据
				{
						for(i=0; i<12; i++)
						{
								OLED_DisChar(1, i+j, 0, SPI752_rbuf_1[i]);   // //在屏幕上打印zigbee通信时rd内存中接收的数据
							j += 6;
						}
						j = 6;
					
					if(SPI752_rbuf_1[0]=='a' && SPI752_rbuf_1[1]=='a')   //判断接收的信号
					{  
						 GPIOSetValue(PORT3, 0, 0);		// PIO3_0 output 0, Turn on LED1   点亮LED1
						for(m=0;m<250000;m++)
						{
							for(n=0;n<20;n++)
							{}
						}						
						GPIOSetValue(PORT3, 0, 1);		// PIO3_0 output 1, Turn off LED1  灭灯
												
					// GPIOSetValue(PORT3, 1, 0);		// PIO3_1 output 0, Turn on LED2    点亮LED2
	        // GPIOSetValue(PORT3, 1, 1);		// PIO3_1 output 1, Turn off LED2		  												
					}
					
					
					if(SPI752_rbuf_1[0]=='d' && SPI752_rbuf_1[1]=='a')
					{
						GPIOSetValue(PORT0, 2, 0);		// PIO0_2 output 0, Turn on FAN   开风扇		  
					}
					
					
					if(SPI752_rbuf_1[0]=='d' && SPI752_rbuf_1[1]=='b')
					{		
						GPIOSetValue(PORT0, 2, 1);		// PIO0_2 output 1, Turn off FAN	  关风扇	  
					}
					
					
					if(SPI752_rbuf_1[0]=='X' && SPI752_rbuf_1[1]=='G')    //修改卡密码
					{	
						while(1)
						{
								if(rfid_flag)    //判断是否刷卡，刷卡时会产生中断将rfid_flag置1
								{
										rfid_flag = 0;
										memcpy(RFID_WRITE_DATA_BLOCK_22,RFID_WRITE_DATA_BLOCK_22_1,10);  
										memcpy(RFID_WRITE_DATA_BLOCK_22+10,SPI752_rbuf_1+2,10);   //将rbuf内存区的数据复制到	
										memcpy(RFID_WRITE_DATA_BLOCK_22+20,RFID_WRITE_DATA_BLOCK_22_2,6); 
										
		            		RFID_Operate((uint8_t *)RFID_WRITE_DATA_BLOCK_22, rbuf);  //改密码
									
										memset(rbuf,sizeof(rbuf),0);   
										//memset(SPI752_rbuf_1,12,0);   
									
									
										memset(&com_s,com_len,0);   //清空message数组
										com_s.a='X';  
										com_s.b='G';					
										memcpy(&com_s.str,"0000000000",10);   //将rbuf内存区的数据复制到	
										send_message_zigbee(&com_s);   //发送数据	
									 // RFID_Test(RFID_WRITE_DATA_BLOCK_22);   //往卡中写数据,修改密码		
									
									rfid_flag = 0;
									break;
								}									
						}
					}
									
				}
			}
			
			
			
			

					if(sel_flag)   //中间键
					{
						sel_flag = 0;	
						
						for(i=0; i<21; i++)
						{
							OLED_DisChar(1, i+j, 0, pass[i]);   // //在屏幕上打印"请输入密码“
							j += 6;
						}
						j = 6;
						
						
						memset(password,4,0);   //清空message数组
						
						while(1)  //输密码
						{
							
							if(esc_flag)     //退出
							{
								esc_flag = 0;
								k=0;
								L=0;
								memset(password,4,0);   //清空message数组								
								break;
							}
							
							
							if(up_flag)   //上
							{
								up_flag = 0;
								password[k]='1';
								k++;												
							}
							
							if(down_flag)    //下
							{
								down_flag=0;
								password[k]='2';
								k++;																					
							}
						
							if(left_flag)    //左
							{
								left_flag=0;
								password[k]='3';
								k++;							
							}
							if(right_flag)   //右
							{
								right_flag=0;
								password[k]='4';
								k++;								
							}
							
							
							if(sel_flag)  //确认键   密码输入完成
							{
								sel_flag=0;
								
								for(i=0; i<4; i++)
								{
									OLED_DisChar(2, i+j, 0, password[i]);   //在屏幕上显示输入的密码
									j += 6;
								}
								j = 6;
						
						
								if(!memcmp(password,"2222",4))   //判断密码  密码正确
								{
									k=0;  //k置0
									L=0;  //l置0
									memset(password,4,0);   //清空password数组
									GPIOSetValue(PORT3, 0, 0);		// PIO3_0 output 0, Turn on LED1   点亮LED1
									for(m=0;m<250000;m++)
									{
										for(n=0;n<30;n++)
										{}
									}						
									GPIOSetValue(PORT3, 0, 1);		// PIO3_0 output 1, Turn off LED1  灭灯
									
									break;
								}
								
								else   //密码错误
								{
									k=0;
									L++;   //输入密码次数
									memset(password,4,0);   //清空message数组
									
								
									for(i=0; i<30; i++)
									{
										OLED_DisChar(1, i+j, 0, passerror[i]);   // //在屏幕上打印密码错误
										j += 6;
									}
									j = 6;
									
							
									if(L==3)   //输入三次错误密码
									{
										
										L=0;
									
										memset(&com_s,com_len,0);   //清空message数组
										com_s.a='e';   
										com_s.b='b';						
										memcpy(&com_s.str,"0123456789",10);   
										send_message_zigbee(&com_s);   //发送数据												
									}
														
									continue;   //重新输入密码								
								}
								
								
							}
							
						}
						
					}
			
			
			if(Light_Test()<30)
			{
				GPIOSetValue(PORT3, 0, 0);		// PIO3_1 output 0, Turn on LED2    点亮LED2  	
			}
			
			
			if(Light_Test()>30||Light_Test()==30)
			{				
	       GPIOSetValue(PORT3, 0, 1);		// PIO3_1 output 1, Turn off LED2		  	
			}
		
			
			//	Temp_Hum_Test();   //显示温湿度
			
			

	} 
   		
 
}

/************************************* End of File *****************************************/
