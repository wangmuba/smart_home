#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <sys/types.h>

struct env{
	int id;
	int tem;
	int hum;
	int light;
	int x;
	int y;
	int z;
};

struct env data;

int serial_open(char *devpath);
int serial_Set(int fd,int speed,int flow_ctrl,int databits,int stopbits,int parity);  
int serial_init(char *devpath);
ssize_t serial_recv_exact_nbytes(int fd, void *buf, size_t count);
ssize_t serial_send_exact_nbytes(int fd,  unsigned char *buf, size_t count);
int serial_exit(int fd);

#endif
