#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include"adc.h"

int adcOpen(char *path)
{
	int adcFd;

	adcFd = open(path, O_RDWR);
	if (adcFd < 0) {
		perror("open adc err");
		return -1;
	}

	return adcFd;
}


int adcRead(int adcFd)
{
	int weight;
	union chan_val cv;

	cv.chan = 3;
	printf("adcFd:%d \r\n",adcFd);
	ioctl(adcFd, FSADC_GET_VAL,&cv);
	weight = (int)90*cv.val/4095.0;
	printf("your weight:%.2fKG \r\n",weight);

	return weight;
}

int adcClose(int adcFd)
{
	close(adcFd);
	return 1;
}

