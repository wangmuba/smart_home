#ifndef _FSPWM_H
#define _FSPWM_H

#define FSPWM_MAGIC	'f'
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

#define FSPWM_START	_IO(FSPWM_MAGIC, 0)
#define FSPWM_STOP	_IO(FSPWM_MAGIC, 1)
#define FSPWM_SET_FREQ	_IOW(FSPWM_MAGIC, 2, unsigned int)

#define DO	262
#define RE	294
#define MI	330
#define FA	349
#define SOL	392
#define LA	440
#define SI	494

#define BEAT	(60000000 / 120)

typedef struct 
{
	int pitch;
	int dimation;
}note;

int doorbell(int sec);

#endif
