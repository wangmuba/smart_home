#ifndef _DATEBASE_H_
#define _DATEBASE_H_

int openDatabase(char *Path);
long selectTable(char *pSql, char *pSelect,char *ret);
int insertTable(char *pSelectTable, char *pName,char *pData);
int updateTable(char *pUpdateTable, char *pName,char *pData);

int CreateTable();
int FindCard(char * id);
int RID(int socketID,char *buffer[]);
int DID(int socketID,char *buffer[]);
int Zhuce(int socketID,char *buffer[]);
int Login(int socketID,char *buffer[]);
int FindTable(const char *buf);
int InsertEnvironment(int tmp,int hum,int light);
int Begin(int socketID,char *buffer[]);

#endif
