#ifndef _ADC_H_
#define _ADC_H_

union chan_val{
	unsigned int chan;
	unsigned int val;
};
#define FSADC_MAGIC	'f'
#define FSADC_GET_VAL	_IOWR(FSADC_MAGIC, 0, union chan_val)

#endif
