#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <termios.h>

#include <error.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>

//#include "tcp_server.h"
#include "serial.h"
#include"sqliteinterface.h"
#include"datebase.h"
#include"doorbell.h"

#define ZGB_DATA_SIZE	36
#define REQ_DATA_SIZE   32
#define HDR_DATA_SIZE   128
#define ZIGBEE_DEV_PATH "/dev/ttyUSB0"

extern int zigbeeFd,tcpServerFd;
extern pthread_mutex_t env_mutex;
int screenshot();

int zigbee_init( char *devpath)
{
	return serial_init(devpath);
}

int zigbee_get_dat(int fd)
{
	printf("zigbee_get_dat\r\n");
	ssize_t ret;
	unsigned char buf[12] = {0};
	
	memset(buf, 0, sizeof(buf));
	ret = serial_recv_exact_nbytes(fd, buf, sizeof(buf));

	printf("rcv buf:%s\r\n",buf);
	if (strstr(buf, "ba") != NULL)
	{
		printf("join in ba\r\n");
		doorbell(1);
		printf("doorbell has been called \n");
		zigbee_put_cmd(fd,  "DOOR_ON");
		printf("DooR_ON\n");
	} 
	else if (strstr(buf, "ca") != NULL)
	{
		printf("RFID:%s\r\n",buf);
		int len = strlen(buf);
		char key[4] = {0};
		char password[20] = {0};
		char ret[20] = {0};
		strncpy(key,buf+2,3);
		strncpy(password,buf+5,len-5);
		printf("len: %d,key:%s,password:%s\r\n",len,key,password);
		if(1 == selectTable("RFID", key,ret))
		{
			printf("ok\r\n");
			zigbee_put_cmd(fd, "DOOR_ON");
		}
		
	} 
	else if (strstr(buf, "ea") != NULL)
	{
		//buf[8] = "ba";
	} 
	else if (strstr(buf, "eb") != NULL)
	{
		//buf[8] = "ba";
		printf("join in eb\r\n");
		warning(1);
		//zigbee_put_cmd(fd,  "");
	} 
	/////刷新数据库
	return ret;
}

int zigbee_put_cmd(int fd,  unsigned char *p)
{
	unsigned char buf[ZGB_DATA_SIZE] = {0};
	memset(buf,0,ZGB_DATA_SIZE);
	int ret;
	if (strstr(p, "DOOR_ON") != NULL)
	{
		memcpy(buf, "aaRENXIAOJUN", 12);
	} 
	else if (strstr(p, "DOOR_OFF") != NULL)
	{
		memcpy(buf, "abRENXIAOJUN", 12);
	} 
	else if (strstr(p, "BUZZ_ON") != NULL)
	{
		memcpy(buf, "baRENXIAOJUN", 12);
	} 
	else if (strstr(p, "BUZZ_OFF") != NULL)
	{
		memcpy(buf, "bb", 2);
	} 
	else if (strstr(p, "CARD_ON") != NULL)
	{
		memcpy(buf, "ca", 2);
	} 
	else if (strstr(p, "CARD_OFF") != NULL)
	{
		memcpy(buf, "cb", 2);
	} 
	else if (strstr(p, "OPEN_FAN") != NULL)
	{
		memcpy(buf, "daRENXIAOJUN", 12);
	} 
	else if (strstr(p, "CLOSE_FAN") != NULL)
	{
		memcpy(buf, "dbRENXIAOJUN", 12);
	} 
	else if (strstr(p, "PASS_ON") != NULL)
	{
		memcpy(buf, "ea", 2);
	} 
	else if (strstr(p, "PASS_OFF") != NULL)
	{
		memcpy(buf, "eb", 2);
	}///
	else if(strstr(p,"XG")!=NULL)
	{
		memcpy(buf, p, 12);
		printf("send buf:%s\r\n",buf);
		ret = serial_send_exact_nbytes(fd, buf, 12);
		return ret;
		
	}
	printf("send buf:%s\r\n",buf);
	ret = serial_send_exact_nbytes(fd, buf, 12);
	if(strstr(buf,"aa") != NULL)
	{
       ret =  screenshot();
	   if(ret == 0)
	   {
		   printf("screenshot success\n");
	   }
	}
	
	return ret;
}

int zigbee_exit(int fd)
{
	return serial_exit(fd);
}

void *thread_zigbee(void *arg)
{
	int ret;
	char request[REQ_DATA_SIZE] = {0};
	char response[HDR_DATA_SIZE] = {0};
	
	unsigned char buf[ZGB_DATA_SIZE];
	int connfd = *(int *)arg;
AA:	zigbeeFd = serial_init("/dev/ttyUSB0");
	if (zigbeeFd == -1)
	{
		sleep(1);
		goto AA;
	}
	else
	{
		fprintf(stdout, "init zigbee success\n");
	}
	//free(arg);
	
	while (1)
	{
		 zigbee_get_dat(zigbeeFd);
	}
}























