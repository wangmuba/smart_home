#ifndef __MODUL_H__
#define __MODUL_H__

int zigbee_init( char *devpath, int baudrate);
int zigbee_get_dat(int fd);
int zigbee_put_cmd(int fd, unsigned char *p);
int zigbee_exit(int fd);
void *thread_zigbee(void *arg);



#endif
