#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtNetwork/QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void setStrData(QString str);
    void setSocket(QTcpSocket *socket);
    void comread();

private slots:

    void on_show_clicked();

    void on_pushButton_9_clicked();


private:
    Ui::Widget *ui;

    QString strData;

    QTcpSocket  *socketCom;
};

#endif // WIDGET_H
