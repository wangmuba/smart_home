#ifndef SMARTHOME_H
#define SMARTHOME_H

#include <QMainWindow>
#include <QTimer>
#include <QtNetwork/QTcpSocket>
#include "pic_record.h"
#include"widget.h"

namespace Ui {
class Smarthome;
}

class Smarthome : public QMainWindow
{
    Q_OBJECT

public:
    explicit Smarthome(QWidget *parent = 0);
    QTcpSocket  *socketCom;
    QTcpSocket  *mypicsocket;
    ~Smarthome();
private slots:
    void msconnected();
    void msdisconnected();
    void updatepic();
    void comread();
    void rev_pic();
    void pic_misconnected();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

private:
    Ui::Smarthome *ui;

    class Widget *weightdialog;
    QTcpSocket  *socket;
    pic_record picture;
    QTimer  *timer;
    QPixmap *pixmap;
    QString str;
};

#endif // SMARTHOME_H
