#ifndef PIC_RECORD_H
#define PIC_RECORD_H
#include <QtNetwork/QTcpSocket>
#include <QDialog>

namespace Ui {
class pic_record;
}

class pic_record : public QDialog
{
    Q_OBJECT

public:
    QTcpSocket *picsocket;
    explicit pic_record(QWidget *parent = 0);
    void receive_picture();
    ~pic_record();

private slots:

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

private:
    Ui::pic_record *ui;
    QPixmap *pixmap;
};

#endif // PIC_RECORD_H
