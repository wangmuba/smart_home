#include "widget.h"
#include "ui_widget.h"
#include<QtCharts/QChartView>
#include<QtCharts/QLineSeries>
#include<QtCharts/QCategoryAxis>
#include<QtCharts/QLineSeries>
#include<QStringList>

using namespace QtCharts;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //strData ="XS-10-20-30-40-50-60-70-70-80-90";
    connect(ui->pushButton_8 ,SIGNAL(clicked(bool)),this, SLOT(on_show_clicked(void)));
    this->socketCom = new QTcpSocket();
    //connect(this->socketCom,SIGNAL(readyRead()),this,SLOT(comread()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_show_clicked()
{
    QByteArray commond = 0;
    commond.append("XS-");
    commond.append(ui->lineEdit->text());
    commond.append("-00");
    this->socketCom->write(commond,strlen(commond));
    qDebug() << commond;
}

void Widget::comread()
{
    int i;
    bool ok;
    QLineSeries *series = new QLineSeries;
    //QString strData ="XS-10-20-30-40-50-60-70-70-80-90";
    QStringList strList = strData.split("-");
    for(i=1;i<=10;i++)
    {
        *series<<QPoint(5*i,strList.at(i).toInt(&ok,10));
    }

    //*series<<QPoint(0,6)<< QPointF(9, 4) << QPointF(15, 20) << QPointF(25, 12) << QPointF(29, 26);
    QChart *chart = new QChart;
    chart->legend()->hide();
    chart->addSeries(series);


    QPen pen(QRgb(0xff0000));
    pen.setWidth(5);
    series->setPen(pen);


    QFont font;
    font.setPixelSize(18);
    chart->setTitleFont(font);
    chart->setTitleBrush(QBrush(Qt::white));
    chart->setTitle("体重变化折线图");


    QLinearGradient backgroundGradient;
    backgroundGradient.setStart(QPointF(0, 0));
    backgroundGradient.setFinalStop(QPointF(0, 1));
    backgroundGradient.setColorAt(0.0, QRgb(0xd2d0d1));
    backgroundGradient.setColorAt(1.0, QRgb(0x4c4547));
    backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    chart->setBackgroundBrush(backgroundGradient);

    QLinearGradient plotAreaGradient;
    plotAreaGradient.setStart(QPointF(0, 1));
    plotAreaGradient.setFinalStop(QPointF(1, 0));
    plotAreaGradient.setColorAt(0.0, QRgb(0x555555));
    plotAreaGradient.setColorAt(1.0, QRgb(0x55aa55));
    plotAreaGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
    chart->setPlotAreaBackgroundBrush(plotAreaGradient);
    chart->setPlotAreaBackgroundVisible(true);




    QCategoryAxis *axisX = new QCategoryAxis;
    QCategoryAxis *axisY = new QCategoryAxis;

    QFont lablesFont;
    lablesFont.setPixelSize(12);
    axisX->setLabelsFont(lablesFont);
    axisY->setLabelsFont(lablesFont);


    QPen axisPen(Qt::blue);
    axisX->setLinePen(axisPen);
    axisY->setLinePen(axisPen);

    QBrush axisBrush(Qt::black);
    axisX->setLabelsBrush(axisBrush);
    axisY->setLabelsBrush(axisBrush);


    axisX->setGridLineVisible(false);
    axisY->setGridLineVisible(false);

    axisX->setShadesPen(Qt::NoPen);
    axisX->setShadesBrush(QBrush(QColor(0x99, 0xcc, 0xcc, 0x55)));
    axisX->setShadesVisible(true);


    axisY->setShadesPen(Qt::NoPen);
    axisY->setShadesBrush(QBrush(QColor(0x99, 0xcc, 0xcc, 0x55)));
    axisY->setShadesVisible(true);


    axisY->append("50KG",50);
    axisY->append("100KG",100);
    axisY->append("150KG",150);
    axisY->append("100KG",200);
    axisX->setRange(0,50);


    axisX->append("1",5);
    axisX->append("2",10);
    axisX->append("3",15);
    axisX->append("4",20);
    axisX->append("5",25);
    axisX->append("6",30);
    axisX->append("7",35);
    axisX->append("8",40);
    axisX->append("9",45);
    axisX->append("10",50);
    axisY->setRange(0,200);

    chart->setAxisX(axisX,series);
    chart->setAxisY(axisY,series);



    QChartView *chartView = new QChartView(chart);
    ui->widget->setMinimumHeight(350);
    ui->widget->setMinimumWidth(500);
    chartView->setParent(ui->widget);
    chartView->setRenderHint(QPainter::Antialiasing);

    chartView->setMinimumWidth(500);
    chartView->setMinimumHeight(350);
    chartView->show();
}

void Widget::on_pushButton_9_clicked()
{
    this->close();
}
void Widget::setStrData(QString str)
{
    this->strData = str;
}

void Widget::setSocket(QTcpSocket *socket)
{
    this->socketCom = socket;
}
