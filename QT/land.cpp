#include "land.h"
#include "ui_land.h"
#include <QMessageBox>
#include <QDialog>
#include <QtNetwork/QTcpSocket>
#include "smarthome.h"

Land::Land(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Land)
{
    ui->setupUi(this);

    this->socket = new QTcpSocket();
    this->socket->connectToHost("192.168.1.207",8888);

    //connect(this->socket,SIGNAL(connected()),this,SLOT(msconnected()));
    //connect(this->socket,SIGNAL(disconnected()),this,SLOT(msdisconnected()));

    ui->lineEdit->setFocus();//光标跳到用户名栏开头处
    ui->lineEdit_2->setEchoMode(QLineEdit::Password);//让输入的密码成小黑点

}

Land::~Land()
{
    delete ui;
}

void Land::on_pushButton_clicked()//登录
{
   // this->socketCom = new QTcpSocket();
   // this->socketCom->connectToHost("192.168.0.120",8888);
   // connect(this->socketCom,SIGNAL(readyRead()),this,SLOT(landread());
    ui->lineEdit->setFocus();

    QByteArray commond = 0;
    commond.append("DL-");
    commond.append(ui->lineEdit->text());
    commond.append("-");
    commond.append(ui->lineEdit_2->text());
    this->socket->write(commond,strlen(commond));

    char  buf[20] = {0};
    memset(buf,0,20);
    this->socket->waitForReadyRead(30000);
    this->socket->read(buf,20);
    this->str =QString::fromLocal8Bit(buf);

    if(this->str.trimmed()==tr("DL-OK"))
    {
       //qDebug() << "OK!";
        this->accept();//发一个信号给另一个界面
    }
    else if(this->str.trimmed()==tr("DL-ER"))
    {
        QMessageBox::warning(this,tr("warning"),tr("用户名和密码错误"),QMessageBox::Yes);//如果不正确，弹出警告信息
        ui->lineEdit->clear();//清空用户名栏
        ui->lineEdit_2->clear();//清空密码栏
        ui->lineEdit->setFocus();//光标跳至用户名栏开头处
    }
}

void Land::on_pushButton_2_clicked()//注册
{
    ui->lineEdit->setFocus();
    QByteArray commond = 0;
    commond.append("ZC-");
    commond.append(ui->lineEdit->text());
    commond.append("-");
    commond.append(ui->lineEdit_2->text());
    this->socket->write(commond,strlen(commond));

    char  buf[20] = {0};
    memset(buf,0,20);
    this->socket->waitForReadyRead(30000);
    this->socket->read(buf,20);
    this->str =QString::fromLocal8Bit(buf);
    if(this->str.trimmed()==tr("ZC-OK"))
    {
        QMessageBox::information(this,tr("information"),tr("注册成功，请您登录"),QMessageBox::Yes);
    }
    else if(this->str.trimmed()==tr("ZC-ER"))
    {
        QMessageBox::warning(this,tr("warning"),tr("该用户名已存在，请重新注册"),QMessageBox::Yes);//如果不正确，弹出警告信息
    }
}


/*void Land::msconnected()
{
    QMessageBox::information(this,tr("information"),tr("连接成功"),QMessageBox::Yes);
}

void Land::msdisconnected()
{
    QMessageBox::warning(this,tr("warning"),tr("连接失败"),QMessageBox::Yes);
}*/
