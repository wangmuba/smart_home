#ifndef LAND_H
#define LAND_H

#include <QDialog>
#include <QtNetwork/QTcpSocket>
namespace Ui {
class Land;
}

class Land : public QDialog
{
    Q_OBJECT

public:
    explicit Land(QWidget *parent = 0);
    ~Land();
    QTcpSocket *socket;
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    /*void msconnected();
    void msdisconnected();*/

private:
    Ui::Land *ui;

    QString str;
  //  QTcpSocket *socket;
};

#endif // LAND_H
