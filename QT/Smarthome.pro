#-------------------------------------------------
#
# Project created by QtCreator 2018-07-26T18:30:23
#
#-------------------------------------------------

QT       += core gui network charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Smarthome
TEMPLATE = app


SOURCES += main.cpp\
        smarthome.cpp \
    land.cpp \
    pic_record.cpp \
    widget.cpp

HEADERS  += smarthome.h \
    land.h \
    pic_record.h \
    widget.h

FORMS    += smarthome.ui \
    land.ui \
    pic_record.ui \
    widget.ui
